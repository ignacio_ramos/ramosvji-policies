FROM openjdk:8-jdk-alpine
VOLUME /tmp
EXPOSE 8083
ADD ./target/ramosvji-policy-0.0.1-SNAPSHOT.jar /ramosvji-policy.jar
ENTRYPOINT ["java","-jar","/ramosvji-policy.jar","com.ramosvji.policy.RamosvjiPolicyApplication"]