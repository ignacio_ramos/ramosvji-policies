package com.ramosvji.policy.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.ramosvji.policy.entity.Policy;
import com.ramosvji.policy.repository.PolicyRepository;
import com.ramosvji.policy.service.PolicyService;

@Service
public class PolicyServiceImpl implements  PolicyService {
	private static final String FIREBASE_URL =
            "https://fcm.googleapis.com/fcm/send";
	private static final String KEY = "AAAA42nT5r8:APA91bEu8QSY72SaBfpT0bcOduj0RSUkv3X43wRmAClvzezPmxZBPy7WJyN_3wPN3kL2kykqloY5MYtzLASno2ELHGt-ppmYXCoNP2CLFj28gl1P-dO8VvfrggoU92au_h3X5u2Ffvx3";
	private static final String TOKEN = "eOYxvGk7-3g:APA91bFIurWtTBuoPmdlogvmohlCKWJxvsNk5S0vb0lo4Z-RG20bhclGmnw2FJKmecCV5TeoCnrh_wU31ayTpiCr0FAjZdsHUQMXRmC9V3q9dQ9f5tXiBo900bQ1cUipBQxdEDj6HmEA";
	
	
	@Autowired
	PolicyRepository repository;
	
	@Override
	public void save(final Policy policy) {
		Policy response = repository.save(policy);
		this.sendToFirebase(response.getId());
	}

	@Override
	public Optional<List<Policy>> getPoliciesByUsername(final String username) {
		return repository.findByUsername(username);
	}

	@Override
	public Policy updatePolicy(final Policy request) {
		if(repository.existsById(request.getId())) {
			System.out.println(request);
			Optional<Policy> result = repository.findById(request.getId());
			result.get().setCar(request.getCar());
			Policy policy = repository.save(result.get());
			return policy;
		}	
		return null;		
	}

	@Override
	public void deletePolicy(final String number) {
		repository.deleteById(number);
	}

	@Override
	public Policy getPolicy(final String id) {
		Optional<Policy> result= repository.findById(id);
		
		return result.get();
	}

	private void sendToFirebase(String policyId) {
		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    headers.add("Authorization", "key=" + KEY);
	    
		Request request = new Request();
		Notification notification = new Notification();
		notification.setText("Póliza");
		notification.setTitle("Contratación "+ policyId);
		notification.setColor("#FF9900");
		notification.setSound("default");
		
		request.setNotification(notification);
		request.setPriority("high");
		request.setTo(TOKEN);
		
		final HttpEntity<Request> entity = new HttpEntity<Request>(request, headers);

		RestTemplate template = new RestTemplate();
		
		template.exchange(FIREBASE_URL, HttpMethod.POST, entity, Response.class);        
        
	}
	
}
