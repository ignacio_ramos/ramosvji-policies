package com.ramosvji.policy.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.ramosvji.policy.entity.Policy;

@Service
public interface PolicyService {
	
	public void save(final Policy policy);
	
	public Optional<List<Policy>> getPoliciesByUsername(final String username);
	
	public Policy updatePolicy(final Policy policy);
	
	public void deletePolicy(final String id);
	
	public Policy getPolicy(final String id);

}
