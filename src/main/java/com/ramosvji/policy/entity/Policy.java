package com.ramosvji.policy.entity;

import java.util.Date;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;

public class Policy {
	
	@Id
	private String id;
	private String username;
	private Car car;
	@CreatedDate
	private Date initialDate;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Car getCar() {
		return car;
	}
	public void setCar(Car car) {
		this.car = car;
	}
	public Date getInitialDate() {
		return initialDate;
	}
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	@Override
	public String toString() {
		return "Policy [id=" + id + ", username=" + username + ", car=" + car + ", initialDate=" + initialDate + "]";
	}
	
}
