package com.ramosvji.policy.entity;

public class Car {
	
	private String brand;
	private String subBrand;
	private String model;
	private String licencePlate;
	private String serial;
	
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getSubBrand() {
		return subBrand;
	}
	public void setSubBrand(String subBrand) {
		this.subBrand = subBrand;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getLicencePlate() {
		return licencePlate;
	}
	public void setLicencePlate(String licencePlate) {
		this.licencePlate = licencePlate;
	}
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	@Override
	public String toString() {
		return "Car [brand=" + brand + ", subBrand=" + subBrand + ", model=" + model + ", licencePlate=" + licencePlate
				+ ", serial=" + serial + "]";
	}

}
