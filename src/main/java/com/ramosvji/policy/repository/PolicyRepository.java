package com.ramosvji.policy.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.ramosvji.policy.entity.Policy;

@Repository
public interface PolicyRepository extends MongoRepository<Policy,String> {
	
	public Optional<List<Policy>> findByUsername(final String username);
}
