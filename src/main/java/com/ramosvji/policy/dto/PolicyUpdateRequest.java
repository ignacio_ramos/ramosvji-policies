package com.ramosvji.policy.dto;

import java.io.Serializable;

public class PolicyUpdateRequest implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private CarDto car;

	public CarDto getCar() {
		return car;
	}

	public void setCar(CarDto car) {
		this.car = car;
	}

	@Override
	public String toString() {
		return "PolicyUpdateRequest [car=" + car + "]";
	}

}
