package com.ramosvji.policy.dto;

import java.io.Serializable;

public class PolicyDtoRequest implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String username;
	private CarDto car;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public CarDto getCar() {
		return car;
	}
	public void setCar(CarDto car) {
		this.car = car;
	}
	@Override
	public String toString() {
		return "PolicyDtoRequest [username=" + username + ", car=" + car + "]";
	}
}
