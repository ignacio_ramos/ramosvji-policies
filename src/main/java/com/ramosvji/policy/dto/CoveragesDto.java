package com.ramosvji.policy.dto;

import java.io.Serializable;
import java.util.List;

public class CoveragesDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<CoverageDto> coverages;

	public List<CoverageDto> getCoverages() {
		return coverages;
	}

	public void setCoverages(List<CoverageDto> coverages) {
		this.coverages = coverages;
	}
}
