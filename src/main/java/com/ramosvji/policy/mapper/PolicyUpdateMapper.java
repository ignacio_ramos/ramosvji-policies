package com.ramosvji.policy.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ramosvji.policy.dto.PolicyDtoRequest;
import com.ramosvji.policy.dto.PolicyUpdateRequest;
import com.ramosvji.policy.entity.Policy;

@Component
public class PolicyUpdateMapper {
	@Autowired
    private ModelMapper modelMapper;
	
	public Policy map(final PolicyUpdateRequest request,final String id, final String username) {
		PolicyDtoRequest dto = new PolicyDtoRequest();
		dto.setCar(request.getCar());
		
		Policy policy = modelMapper.map(dto, Policy.class);
		policy.setId(id);
		policy.setUsername(username);
		
		return policy;
	}

}
