package com.ramosvji.policy.impl;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ramosvji.policy.controller.PolicyController;
import com.ramosvji.policy.dto.PolicyDtoRequest;
import com.ramosvji.policy.dto.PolicyUpdateRequest;
import com.ramosvji.policy.entity.Policy;
import com.ramosvji.policy.mapper.PolicyUpdateMapper;
import com.ramosvji.policy.service.PolicyService;

@RestController
@RequestMapping(path="/ramosvji/api")
//@CrossOrigin(origins = { "http://localhost:4200" })
public class PolicyControllerImpl implements PolicyController{
	@Autowired
    private ModelMapper modelMapper;
	
	@Autowired
	private PolicyUpdateMapper updateMapper;
	
	@Autowired
	private PolicyService service;
	
	@PostMapping(path="/v01/policy")
	@Override
	public ResponseEntity<Void> savePolicy(final @RequestBody PolicyDtoRequest request) {
		Policy policy = modelMapper.map(request, Policy.class);
		service.save(policy);
		
		return new ResponseEntity<Void>(null, new HttpHeaders(), HttpStatus.OK);
	}

	@GetMapping(path="/v01/policies/{username}")
	@Override
	public ResponseEntity<Optional<List<Policy>>> getPoliciesByUsername(
			final @PathVariable(value="username") String username) {
		
		
		Optional<List<Policy>> policies = service.getPoliciesByUsername(username);
				
		return new ResponseEntity<Optional<List<Policy>>>(policies, new HttpHeaders(), HttpStatus.OK);
	}

	@PatchMapping(path="/v01/policy/{id}/{username}")
	@Override
	public ResponseEntity<Policy> updatePolicy(final @RequestBody PolicyUpdateRequest request,
			final @PathVariable(value="id") String id, final @PathVariable(value="username") String username) {
		System.out.println(request);
		System.out.println(username);
		System.out.println(id);
		
		Policy policy = updateMapper.map(request, id, username);
		
		policy =  service.updatePolicy(policy);
		
		return new ResponseEntity<Policy>(policy, new HttpHeaders(), HttpStatus.OK);
	}
	
	@GetMapping(path="/v01/policy/{id}")
	@Override
	public ResponseEntity<Policy> getPlolicy(final @PathVariable(value="id") String id) {
		
		Policy policy = service.getPolicy(id);
		
		return new ResponseEntity<Policy>(policy, new HttpHeaders(), HttpStatus.OK);
	}

}
