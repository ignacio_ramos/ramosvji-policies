package com.ramosvji.policy.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ramosvji.policy.dto.PolicyDtoRequest;
import com.ramosvji.policy.dto.PolicyUpdateRequest;
import com.ramosvji.policy.entity.Policy;

@RestController
@RequestMapping(path="/ramosvji/api")
public interface PolicyController {
	
	@PostMapping(path="/v01/policy")
	public ResponseEntity<Void> savePolicy(final @RequestBody PolicyDtoRequest request);
	
	@GetMapping(path="/v01/policies/{username}")
	public ResponseEntity<Optional<List<Policy>>> getPoliciesByUsername(
			final @PathVariable(value="username") String username);
	
	@GetMapping(path="/v01/policy/{id}")
	public ResponseEntity<Policy> getPlolicy(final @PathVariable(value="id") String id);
	
	@PatchMapping(path="/v01/policy/{id}/{username}")
	public ResponseEntity<Policy> updatePolicy(final @RequestBody PolicyUpdateRequest request,
			final @PathVariable(value="id") String id, final @PathVariable(value="username") String username);
}
